import os
import time

import requests

os.environ['http_proxy'] = "http://w3账号:密码@proxycn2.huawei.com:8080/"
os.environ['https_proxy'] = "http://w3账号:密码@proxycn2.huawei.com:8080/"
try:
    from urllib import urlencode
except ImportError:
    from urllib.parse import urlencode

repo_owner = 'src-openeuler'  # 获取那个组织的仓库
requests.packages.urllib3.disable_warnings()
body = "/source_scan"
data = {"access_token": 'token实际的值', "body": "/source_scan"}
for i in range(10, 101):  # 从第几页开始扫描
    comment_pr_url = "https://gitee.com/api/v5/orgs/{}/repos?type=all&page={}&per_page=100".format(repo_owner, i)
    res = requests.get(comment_pr_url, verify=False, timeout=50)
    print("获取仓库")
    root_dict = res.json()
    for j in range(0, 100):
        repo = root_dict[j]["name"]
        print("页数")
        print(i)
        print("仓库名为" + repo)
        comment_pr_url = "https://gitee.com/api/v5/repos/{}/{}/pulls/{}/comments".format(repo_owner, repo, 1)
        res = requests.post(comment_pr_url, data, timeout=50, verify=False)
        print("评论/source_scan")
        comment_pr_url = "https://gitee.com/api/v5/repos/{}/{}/pulls/{}/comments?per_page=20&direction=desc". \
            format(repo_owner, repo, 1)
        res = requests.get(comment_pr_url, data, timeout=50, verify=False)
        if res.status_code == 200:
            while res.json()[0]["user"]["name"] == 'anyuting':
                time.sleep(30)
                res = requests.get(comment_pr_url, data, timeout=50, verify=False)
        print("已得到" + repo + "的报告")
