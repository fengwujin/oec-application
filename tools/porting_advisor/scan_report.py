import csv
import os

path = 'C:\\Users\Desktop\\report-master'  # 要扫描的文件夹，注意不要包含中文
files = os.listdir(path)  # 获取的文件夹列表
title = ["项目", "是否需要迁移", "Number of Scanned Files", "C/C++/Fortran/Makefile/CMakeLists.txt/Automake",
         "pure assembly files", "Go files", "python files", "java files", "scala files",
         "Estimated transplant workload"]

with open(r'C:\\Users\Desktop\\report.csv', 'a', newline="", encoding="UTF-8-SIG") as f:  #写入title
    writer = csv.writer(f)
    writer.writerow(title)


def splitCsvFiles():
    for file in files:
        dirpath = 'C:\\Users\Desktop\\report-master\\{}\\porting-advisor.csv'.format(file)
        print(dirpath)
        f = open(dirpath)
        csv_reader = csv.reader(f)
        fields = [file]
        begin = 0
        for line in csv_reader:  #判断是否需要迁移，
            if begin == 1:
                fields.append(line)
            if 'Estimated transplant' in line:
                begin = 0
                break
            if 'Source Need Migrated: NO' in line:
                fields.append('Source Need Migrated: NO')
                break
            if 'Source Need Migrated: YES' in line:
                begin = 1
                fields.append('Source Need Migrated: YES')
            if 'Architecture-related Dependencies:' in line or 'Source files scan details are as follows:' in line:
                fields.pop(len(fields) - 1)
                fields.pop(len(fields) - 1)
                begin = 0
                break
        with open(r'C:\\Users\Desktop\\report.csv', 'a', newline="") as f:
            writer = csv.writer(f)
            writer.writerow(fields)


if __name__ == '__main__':
    splitCsvFiles()
